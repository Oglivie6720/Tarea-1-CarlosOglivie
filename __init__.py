import os
from app.sucursal import Sucursal

if __name__ == "__main__":

    while True:
        lsucur = []

        print('\nREGISTRO DE SUCURSALES Y PRODUCTOS')
        print("1. Registrar una Sucursal")
        print("2. Cerrar Sucursal")
        print("3. Agregar Producto")
        print("4. Modificar Producto")
        print("5. Eliminar Producto")
        print("6. Ver Sucursal")
        print("7. Salir")

        try:
            opcion = int(input("Opcion: "))
        except:
            opcion = -1

        if opcion == 1:
            print("\n--Registrar una Sucursal--")
            n = input("Nombre de la Sucursal: ")
            try:
                f = open(n + ".txt", "w+")
                f.close()
            except:
                print("ERROR: La sucursal no pudo ser registrada.")
        elif opcion == 2:
            print("\n--Cerrar Sucursal--")
            n = input("Nombre de la Sucursal: ")
            if os.path.exists(n + ".txt"):
                os.remove(n + ".txt")
            else:
                print("ERROR: No puedo cerrarse la sucursal no existe o no se encuenntra registrada.")
        elif opcion == 3:
            print("\n--Agregar Producto a la Sucursal--")
            while True:
                n = input("\nProducto : ")

                e = input("Categoria: ")
                while True:
                    try:
                        p = int(input("Cantidad: "))
                    except:
                        p = -1
                    if p > 0:
                        break
                    else:
                        print("ERROR: Cantidad no valida")
                    while True:
                        try:
                            c = int(input("Precio: "))
                        except:
                            c = -1
                        if c > 0:
                            break
                        else:
                            print("ERROR: Precio no valida")
                lsucur.append(sucursal(n, e, c, p))

                while True:
                    seguir = input("Desea continuar? (S/N): ").upper()
                    if seguir != "N" and seguir != "S":
                        print("Seleccion incorrecta.")
                    else:
                        break
                if seguir == "N":
                    break
                elif seguir == "S":
                    pass
            while True:
                try:
                    n = input("Nombre de la sucursal ")
                    f = open(n + ".txt", "a")
                    for suc in lsucur:
                        f.write(str(suc.producto) + "," + str(suc.Categoria)
                                + "," + str(suc.Precio) + "," + str(suc.Cantidad) + "\n")
                    f.close()
                    break
                except:
                    print("ERROR: Sucursal no registrada.")
        elif opcion == 4:
            sucursal_temp = []
            print('Modificar Producto')
            while True:
                try:
                    ne = input("Nombre de laSucursal : ")
                    f = open(ne + ".txt", "r")
                    linea = f.readline()
                    i = 1
                    while linea:
                        sucursal_temp.append(linea)
                        data = linea.split(",")
                        print(str(i) + ". " + data[0])
                        linea = f.readline()
                        i += 1
                    f.close()
                    break
                except:
                    print("ERROR: Sucursal no registrada.")

            while True:
                try:
                    cambio = int(input("Indique el numero del producto en el indice: "))
                except:
                    cambio = -1
                if cambio > 0:
                    break
                else:
                    print("ERROR: Numero del producto erroneo")

            n = input("\nProducto: ")
            e = input("Categoria: ")
            while True:
                try:
                    c = int(input("Precio: "))
                except:
                    c = -1
                if c > 0:
                    break
                else:
                    print("ERROR: Precio invalido")
            while True:
                try:
                    p = int(input("Cantidad: "))
                except:
                    p = -1
                if p > 0:
                    break
                else:
                    print("ERROR: Cantidad Invalida")

            sucursal_temp[cambio - 1] = str(n) + "," + str(e) + "," + str(c) +"," + str(p) +  "\n"

            while True:
                try:
                    f = open(ne + ".txt", "w")
                    for producto in sucursal_temp:
                        f.write(producto)
                    f.close()
                    break
                except:
                    print("ERROR: Sucursal no existe.")
        elif opcion == 5:
            sucursal_temp = []
            print('ELIMINAR PRODUCTO')
            while True:
                try:
                    ne = input("Nombre de la sucursal: ")
                    f = open(ne + ".txt", "r")
                    linea = f.readline()
                    i = 1
                    while linea:
                        sucursal_temp.append(linea)
                        data = linea.split(",")
                        print(str(i) + ". " + data[0])
                        linea = f.readline()
                        i += 1
                    f.close()
                    break
                except:
                    print("ERROR: sucursal no existe.")

            while True:
                try:
                    cambio = int(input("Indique el numero del Producto: "))
                except:
                    cambio = -1
                if cambio > 0:
                    break
                else:
                    print("ERROR: El codigo del producto es invalido")

            sucursal_temp.pop(cambio - 1)

            while True:
                try:
                    f = open(ne + ".txt", "w")
                    for producto in sucursal_temp:
                        f.write(producto)
                    f.close()
                    break
                except:
                    print("ERROR: Sucursal no existe.")
        elif opcion == 6:
            print('\n--Ver Productos en Sucursal--')
            while True:
                try:
                    n = input("Sucursal: ")
                    f = open(n + ".txt", "r")
                    linea = f.readline()
                    while linea:
                        data = linea.split(",")
                        print(data[0] + "  " + data[1] + " " + data[2] + " " + data[3])
                        linea = f.readline()
                    f.close()
                    break
                except:
                    print("ERROR: Sucursal inexistente.")
        elif opcion == 7:
            exit(0)
        else:
            print("Opcion invalida")